import System.IO

hangman :: IO ()
hangman = do putStrLn "Think of a word"
             word <- getLine'
             putStrLn ("You have " ++ show (length word) ++ " chances to guess")
             play word (length word)

getLine' :: IO String
getLine' = do x <- getChar'
              if x == '\n' then
                do putChar x
                   return []
              else
                do putChar '-'
                   xs <- getLine'
                   return (x:xs)

getChar' :: IO Char
getChar' = do hSetEcho stdin False
              x <- getChar
              hSetEcho stdin True
              return x
play :: String -> Int -> IO ()
play word n = if n < 1 then putStrLn "You lost"
  else
    do putStrLn "Try to guess it"
       guess <- getLine
       if guess == word then putStrLn "You won"
       else
         do putStrLn (match word guess)
            play word (n - 1)

match :: String -> String -> String
match word guess = [if elem i guess then i else '-' | i <- word]
